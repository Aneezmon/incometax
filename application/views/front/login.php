<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Page</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="<?php echo base_url();?>css/default.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript">
	function check()
	{
		if(document.getElementById('uname').value=="")
		{
			alert("Please enter the Username");
			document.getElementById('uname').focus();
			return false;
		}
		if(document.getElementById('psd').value=="")
		{
			alert("Please enter the Password");
			document.getElementById('psd').focus();
			return false;
		}
	}
	
</script>
</head>

<body>
	 <?php echo form_open('control/login',array('id' => 'formLogin','name' => 'formLogin')); ?>
<div id="header" class="container">
	<div id="logo">
		<h1><a href="#">Income Tax | Login</a></h1>
	</div>
	
</div>
<div id="page">
	<div class="container1"  style="text-align: center";>
		<div class="innercontainer">
                    
		<table align="center" width="300" cellspacing="10">
		<?php	if(@$error)
		{
		?>
   		  <tr>
			<th height="29" colspan="2" align="center" valign="top" id="error"><?php echo @$error?></th>
		  </tr>
		 <?php
		 }
		 ?>
			<tr><td>Username</td><td><input type="text" name="username" value="" class="textbox" id="uname" /></td></tr>
			<tr><td>Password</td><td><input type="password" name="password" value="" class="textbox" id="psd" /></td></tr>
                        <tr><td colspan="2" align="center"><input type="submit" name="login" value="Login" 
                        	style="background-color:#4C4532;border-radius: 8px; margin-left: 150px; color: #FFFFFF;
    padding: 4px 13px;" onclick="return check();" /></td></tr>
		</table>
                   
		</div>
		
	</div>
</div>
<?php echo form_close(); ?>
<div id="copyright" class="container">
	<p>Copyright (c)  All rights reserved DPI.</p>
	<?php //echo form_validation_errors();?>
</div>
</form>
</body>
</html>