<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Password Change</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="<?php echo base_url();?>css/default.css" rel="stylesheet" type="text/css" media="all" />
<script src="<?php echo base_url(); ?>js/gen_validatorv31.js" type="text/javascript"></script>

<script type="text/javascript">
function valid(o,w){
o.value = o.value.replace(valid.r[w],'');
}
valid.r={
'special':/[\W]/g,
'quotes':/['\''&'\"']/g,
'numbers':/[^\d]/g
}
</script>
</head>
<body>
	 <?php echo form_open('control/psdchange',array('id' => 'formLogin','name' => 'formLogin')); ?>
<div id="header" class="container">
	<div id="logo">
		<h1><a href="#">Income Tax | Change Password</a></h1>
	</div>
	
</div>
<div id="page">
	<div class="container1"  style="text-align: center";>
		<div class="innercontainer">
                    
		<table align="center" width="300" cellspacing="5" >
			<?php	if(@$error)
			{?>
   		  	<tr>
				<th height="29" colspan="2" align="center" valign="top" id="error"><?php echo @$error?></th>
		  	</tr>
<?php 		 } ?>
			<tr>
				<td>Old Password</td>
				<td><input type="password" name="password" value="" class="textbox" /></td>
			</tr>
			<tr>
				<td>New Password</td>
				<td><input type="password" name="newpassword" value="" class="textbox" /></td>
			</tr>
			<tr>
				<td>Confirm Password</td>
				<td><input type="password" name="confirmpassword" value="" class="textbox" /></td>
			</tr>
			<tr>
				<td>Email ID</td>
				<td><input type="text" name="email" value="" class="textbox" /></td>
			</tr>
			<tr>
				<td>Phone Number</td>
				<td><input type="text" name="phone" value="" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" class="textbox" onkeyup="valid(this,'numbers')" onKeyPress="valid(this,'numbers')" /></td>
			</tr>
            <tr>
            	<td colspan="2" align="center">
            		<input type="submit" name="submit" value="Submit" style="background-color:#4C4532;border-radius: 8px; margin-left: 150px; color: #FFFFFF;
   					 padding: 4px 13px;" />
    			</td></tr>
		</table>
                
		</div>
		
	</div>
</div>
<?php echo form_close(); ?>
<div id="copyright" class="container">
	<p>Copyright (c)  All rights reserved DPI.</p>
</div>
</form>

<script  language="javascript" type="text/javascript">
 var frmvalidator = new Validator("formLogin");
 frmvalidator.addValidation("password","req","Please Enter the Password!");
 
 var frmvalidator = new Validator("formLogin");
 frmvalidator.addValidation("newpassword","req","Please Enter the New Password!");
 
 var frmvalidator = new Validator("formLogin");
 frmvalidator.addValidation("confirmpassword","req","Please Enter the Confirm Password!");
 
 var frmvalidator = new Validator("formLogin");
 frmvalidator.addValidation("email","req","Please Enter the Email Id!");
 
 var frmvalidator = new Validator("formLogin");
 frmvalidator.addValidation("email","email","Please Enter a Valid Email Id!");
 
 var frmvalidator = new Validator("formLogin");
 frmvalidator.addValidation("phone","req","Please Enter Phone Number!");
         
 </script>
</body>
</html>