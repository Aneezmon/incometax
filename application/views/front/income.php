<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2013 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2013 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.9, 2013-06-02
 */

/** Error reporting */
error_reporting(E_ERROR);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

require_once '../Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$objPHPExcel->setActiveSheetIndex();
$con=mysql_connect('localhost','root','root');
$s=mysql_select_db('income',$con);
//$seritem=34500;
//$deatils=mysql_query("select * from tax_calc where pan='$seritem'");
//$deatils1=mysql_query("select * from tax_input where pan='$seritem'");
//$deatils2=mysql_query("select * from tax_deduct where pan='$seritem'"); 
//$deatils3=mysql_query("select * from tax_employee where pan='$seritem'");

$deatils=mysql_query("select * from  tax_input ti,tax_deduct td,tax_employee te where ti.pan=td.pan and td.pan=te.pan ");
$i=2;$j=3;$k=0;

//$deat=mysql_query("select * from  tax_calc tc where pan=
$total=0;$hratotal=0;$salary=0;$conatotal=0;$hbatotal=0;$lictotal=0;$gpftotal=0;$slitotal=0;$gistotal=0;$fbstotal=0;$taxtotal=0;

 while($row=$rr=$res3=$res=mysql_fetch_array($deatils))
    { 
        $i++;
        $j++;
        $k++;
//      $med=$row['mediclaim'];   
     $tr=$row['treat'];
             $inc=$row['incur'];
            $lon=$row['loan'];
             $dle=$row['disable'];
                $nsc1=$row['nsc'];
                $rel=$row['releif'];
                $unit=$row['unit'];
                $plan=$row['plan'];
               $mutual=$row['mutual'];
               $scheme=$row['scheme'];
                $fee=$row['fee'];
               $house=$row['house'];
                $eligible=$row['eligible'];
foreach($deatil as $row)
{
        $total=$total+$row['gross'];
       $hratotal=$hratotal+$row['hra'];
       $salary=$salary+$row['bp']+$row['da'];
       $conatotal=$conatotal+$row['cona'];
        $hbatotal=$hbatotal+$row['Interest'];
       $lictotal=$lictotal+$row['lic'];
      $gpftotal=$gpftotal+$row['gpf'];
       $slitotal=$slitotal+$row['sli'];
       $gistotal=$gistotal+$row['gis'];
       $fbstotal=$fbstotal+$row['fbs'];
       $taxtotal=$taxtotal+$row['tax'];
        $plitotal=$plitotal+$row['pli'];
       $htatotal=$htatotal+$row['hta'];
       $gpaistotal=$gpaistotal+$row['gpais'];
   
}
 foreach($deatils3 as $row)
    {     
    $t_income=$total+($row['leave'])+($row['fest_allo'])+($row['paygpf'])+($row['daarrier'])+($row->daarrier1)+($row['medical'])+($row['paycash']);
    $act_rent_paid=($row['rent'])-($salary/10);
    $proftax=($row['profe1'])+($row['profe2']);
    $perc_salary=($salary*40)/100;
    $total_hba=$hbatotal+($row['interhba']);
    $least=0;$balance=0;$less=0;
    $booklic=$row['licbook'];
    $togpf=$row['paygpf'];
    $arrier=$row['daarrier'];
    $arrier1=$row['daarrier1'];
    $arriertotal=$arrier+$arrier1;
    if($hratotal<$act_rent_paid)
    $least=$hratotal;
    elseif($act_rent_paid<$perc_salary)
    $least=$act_rent_paid;
    else 
    $least=$perc_salary; 
    $less=$least;
    $less=round($less);
    if($less<0)
    {
        $balance=$t_income;
        $less=0;
    }
    else
        $balance=$t_income-$less;
    $netsalary=$balance-($conatotal+$proftax);
    $grossincome=$netsalary-($total_hba+($row['other']));
    }   
    
  foreach($deatil2 as $row)
            {  
     $med=$row['mediclaim'];
       if(($row['mediclaim'])>20000)
           $med=20000;
       
       
       
   $totaldeduct=($row['mediclaim'])+($row['treat'])+($row['incur'])+($row['loan'])+($row['disable']);
    $deduct=$grossincome- $totaldeduct;
    $licdeduct=$lictotal+($booklic);
    $gpfdeduct=$gpftotal+$togpf+$arrier+$arrier1;
    $totalsection=0;$totalincome=0;
    $totalsection=$totalsection+$licdeduct+$gpfdeduct+$slitotal+$gistotal+$fbstotal+$gpaistotal+($row->nsc)+($row->deposit)+($row->unit)+($row->plan)+($row->mutual)+($row->scheme)+($row->fee)+($row->house)+($row->eligible)+($row->fund);
    $totalsection1=$totalsection;
    if($totalsection1>100000)
            $totalsection1=100000;
    $totalincome=$deduct-$totalsection1;
     $totincome=$totalincome;
     $rem=$totincome%10;
     $rebate=0;$exce=0;
    if($rem<5)
     $totincome=$totincome-$rem;
    else
      $totincome=$totincome+(10-$rem);
    if($totincome<=200000)
        $excess="nil";$excess="nil"; $excess1="nil"; $excess2="nil";
    if($totincome>=200000 && $totincome<=500000)
       {
        $excess=(($totincome-200000)*10)/100;
        $exce=$excess;$rebate=2000;
        
       }
       else if($totincome>500000 && $totincome<=1000000)
       {
           $excess1=((($totincome-500000)*20)/100)+30000;
           $exce=$excess1;$rebate=0;
       }
       else if($totincome>1000000)
       {
           $excess2=((($totincome-1000000)*30)/100)+130000;
           $exce=$excess2;$rebate=0;
       }
       $surcharge=0;
       if($totincome>10000000)
       {
           $surcharge=((($totincome)*10)/100);
       }
       else
            $surcharge=0;
       $sur=$surcharge+$exce;
       $cess=($sur*3)/100;
       $roundcess=round($cess);
        if($totincome>=200000 && $totincome<=500000)
       {
           $sur=$excess-$rebate;
           $cess=($sur*3)/100;
           $roundcess=round($cess);
           $totaltax=$cess+$sur;
       }
       $totaltax=$roundcess+$sur;
       $roundtotaltax=round($totaltax);
       $bal=$roundtotaltax-($row['releif']);
      // $bal=$bal-$rebate;
       if($bal<=0)
           $bal='Nil';
       $tobepaid=$bal-$taxtotal;
      
       if($rebate>=$excess && $rebate>0)
       {    $rebate=$excess;
            $roundcess='Nil';
            $bal='Nil';
            $totaltax='Nil';
             $sur='Nil';
        }
            }        
     $totalinc=$netsalary+$total_hba; 
     $sectionall=$med+$tr+$inc+$lon+$dle;
     $deductible=$totalsection+$sectionall;
     $taxable=$totalinc-$deductible;
     $taxincome=$totalinc-$taxable;
                     
             
     
     $name=$res['employee_name'];	      
     $pan= $res['pan'];
     $desig=$res['designation'];
     $mont= "april";
     $monthend="march";
     
     $objPHPExcel->setActiveSheetIndex(0)
   ->setCellValue("A".$i,$k)
   ->setCellValue("B".$i,$pan)
   ->setCellValue("C".$i,$name)
   ->setCellValue("D".$i,$desig)
//   ->setCellValue("E")
   ->setCellValue("F".$i,$mont)
   ->setCellValue("G".$i,$monthend)
   ->setCellValue("H".$i,$t_income)
   ->setCellValue("I".$i,$proftax)
   ->setCellValue("J".$i,$less)
   ->setCellValue("K".$i,$conatotal)
   ->setCellValue("L".$i,$netsalary)
   ->setCellValue("M".$i,$total_hba)
   ->setCellValue("N".$i,$grossincome)
   ->setCellValue("O".$i,$totalsection)
   ->setCellValue("P".$i,$sectionall)
   ->setCellValue("Q".$i,$deductible)
   ->setCellValue("R".$i,$totincome)
   ->setCellValue("S".$i,$sur)
   ->setCellValue("T".$i,$surcharge)
   ->setCellValue("U".$i,$roundcess)
   ->setCellValue("V".$i,$rel)  
   ->setCellValue("W".$i,$bal) 
   ->setCellValue("X".$i,$tobepaid); 
    }


//
//while($det=mysql_fetch_array($deatils3))
//{
//  $ename=$det['employee_name'];  
//}
//while($yy=mysql_fetch_array($deatils))
// { 
//   $pan=$yy['pan'];
//   $year=$yy['year'];
// }
//
// while($rr=mysql_fetch_array($deatils2))
// {
//  $med=$rr['mediclaim'];   
//  $tr=$rr['treat'];
//  $inc=$rr['incur'];
//  $lon=$rr['loan'];
//  $dle=$rr['disable'];
//  $nsc1=$rr['nsc'];
//  $rel=$rr['releif'];
//  $unit=$rr['unit'];
//  $plan=$rr['plan'];
//  $mutual=$rr['mutual'];
//  $scheme=$rr['scheme'];
//  $fee=$rr['fee'];
//  $house=$rr['house'];
//  $eligible=$rr['eligible'];
//  $fund=$rr['fund'];
//  }
// while($rr1=mysql_fetch_array($deatil))
// {
//     $oth=$rr1['other'];
// }
// $total=0;$hratotal=0;$salary=0;$conatotal=0;$hbatotal=0;$lictotal=0;$gpftotal=0;$slitotal=0;$gistotal=0;$fbstotal=0;$taxtotal=0;
      

//$objPHPExcel->setActiveSheetIndex(0)
//$objPHPExcel->getActiveSheet()->getStyle('A3:X3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A2:X2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
// $objPHPExcel->getActiveSheet()->getColumnDimension("D4")->setWidth(50);
// $objPHPExcel->getActiveSheet()->getRowDimension("2")->setRowHeight(100);
$objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getAlignment()->setTextRotation(90);
$objPHPExcel->getActiveSheet()->getStyle("A2:Z2")->getAlignment()->setTextRotation(0);
$objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->applyFromArray(array("font" => array( "bold" => true)));
// $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);




$objPHPExcel->getActiveSheet()->mergeCells("C1:D1");
$objPHPExcel->getActiveSheet()->mergeCells("C2:D2");
$objPHPExcel->getActiveSheet()->mergeCells("F2:G2");
$objPHPExcel->getActiveSheet()->mergeCells("F1:G1");


//$objPHPExcel->getActiveSheet()->mergeCells("A2:T2");
//$objPHPExcel->getActiveSheet()->mergeCells("A3:T3");
$objPHPExcel->getActiveSheet()->setTitle('Income');
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue("A1", "Employee Serial No")
->setCellValue("B1", "Employee PAN")
->setCellValue("C1", "NAME")
->setCellValue("D1", "")         
->setCellValue("E1", "Write 'W' for Woman, 'S' for Senior Citizen and 'G' for others")
->setCellValue("F1", "Date from which employed with current employer")
->setCellValue("H1", "Total amount of salay (Includes wages, annuity, pension, gratuity, fees, commission, bonus, repayment of amount deposited etc)")
->setCellValue("I1", "Total Deduction under section 16(ii) and 16(iii) (Specify each deduction separtely)")
->setCellValue("J1", "Total Deduction under section  16(iii) (Specify each deduction separtely)")
->setCellValue("K1", "Conveyance Allowance")
->setCellValue("L1", "Income Chargeable under the head Salaries(Column 332 minus 333)")
->setCellValue("M1", "Income (including loss from housing property) under  any head other than the head Salaries offered for TDS[section 192(2B)]")
->setCellValue("N1", "Gross total income (Total of Columns 334 and 335)")
->setCellValue("O1", "Aggregate amount of deductions under sections 80C, 80CCC and 80CCD (Total to be limited to amount specified in section 80CCE)")
->setCellValue("P1", "Amount deductible under any other provision(s) of Chapter VI-A")
->setCellValue("Q1", "Total Amount deductible under Chapter VI-A(Total of Columns 337 and 338)")
->setCellValue("R1", "Total taxable income(340)(Columns 336 minus column 339)")
->setCellValue("S1", "Total tax - (I) income-tax on total income")
->setCellValue("T1", "(ii) Surcharge")
->setCellValue("U1", "(iii) Education Cess")
->setCellValue("V1", "Income tax Relief under section 89, when salary etc., is paid in arrear or in advance")
->setCellValue("W1", "Net tax payable")
->setCellValue("X1", "Total amount of tax deducted at source for the whole year(aggregate of the amount in column 322 of Annexure I for all the four quarters in respect of each employee)");
//->setCellValue("Y1", "Shortfall in tax deduction(+)/Excess tax deduction(-)[Column 345 minus column 346]");
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(400);
//$objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension("A1")->setRowHeight(150);
$objPHPExcel->getActiveSheet()->getStyle("A1:W1")->getAlignment()->setWrapText(TRUE);
$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(35.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("C4")->setWidth(30.24);
$objPHPExcel->getActiveSheet()->getColumnDimension("C4")->setWidth(30.24);
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue("A2","(327)")
->setCellValue("B2", "(328)")
->setCellValue("C2", "(329)")        
->setCellValue("E2", "(331)")
->setCellValue("F2", "(332)")
->setCellValue("H2", "(333)")
->setCellValue("I2", "(334)")
->setCellValue("J2", "(335)")
->setCellValue("K2", "(336)")
->setCellValue("L2", "(337)")
->setCellValue("M2", "(338)")
->setCellValue("N2", "(339)")
->setCellValue("O2", "(340)")
->setCellValue("P2", "(341)")
->setCellValue("Q2", "(342)")
->setCellValue("R2", "(343)")
->setCellValue("S2", "(344)")
->setCellValue("T2", "(345)")
->setCellValue("U2", "(346)");




header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="IDMI.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
