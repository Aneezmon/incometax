<form name="frminsert" action="" method="post" id="frminsert" class="frminsert">

    <div class="container">
        <div style="margin-right:161px"><b>NAME:</b> <input type='text' name='name' id='name' class="name" value="<?php echo $this->session->userdata('Name');?>">
            <b>DESIGNATION:</b><input type='text' name='desig' id='desig' class="desig" value="<?php echo $this->session->userdata('Desig');?>">
            <b>USER:</b><select name="user" id="user" class="user">
                                   <option value="">--SELECT--</option>
                                   <option value="Ministerial">Ministerial</option>
                                   <option value="SDO">SDO</option></select>
        </div>
        <div style="margin-right:114px"><b>Gross Salary Income(includes Salary D A,H R A,C C A,Interim Relief,O T Allowance Deputation Allowance,Medical Allowance etc:)</b></div>
            
            <div>
                <b>PAN NO:</b> <input type='text' name='pan' id='pan' class="pan" value="<?php echo $this->session->userdata('panNumber');?>">
                &nbsp;<b>Financial Year</b> <select name='year' id='year' class="year">
                    <?php 
                        $d=date('Y');
                    for($i=$d,$j=$i+1;$i>2000;$i--,$j--)
                        echo "<option value='".$i."-".$j."'>".$i."-".$j."</option>"
                        ?>
                </select>
            </div>
            
                <table id='tabgross' class="tabgross">
                    <tr>
                      <td id="tdata"><b>Month</b></td>
                      <td id="tdata"><b>BP</b></td>
                      <td id="tdata"><b>CLASS</b></td>
                      <td id="tdata"><b>DA</b></td>
                      <td id="tdata"><b>HRA</b><br></td> 
                      
                    </tr>
                    <tr id='grossinput' class="grossinput">
                        <td><select name='month' id='month' class="month">
                            <option value="">--SELECT--</option> 
                            <option value="1">March</option>
                            <option value="2">April</option>
                            <option value="3">May</option>
                            <option value="4">June</option>
                            <option value="5">July</option>
                            <option value="6">August</option>
                            <option value="7">September</option>
                            <option value="8">October</option>
                            <option value="9">November</option>
                            <option value="10">December</option>
                            <option value="11">January</option>
                            <option value="12">February</option>
                            </select>
                        </td>
                        <td>
                        <input type="text" name="bp" class="bp" id="bp" value="" /></td>
                        <td><select name="city" id="city" class="city">
                                   <option value="">--SELECT--</option>
                                   <option value="1">B2 Class</option>
                                   <option value="2">C Class</option>
                                   <option value="3">Not in B2 and C </option>
                                   <option value="4">Other</option>
                            </select>
                        </td>
                    <td><input type="text" name="da" id="da" class="da" /></td>
                    <td><input type="text" class="hra" name="hra" id="hra"/></td>
                    </tr>
                    
                    <tr>
                      <td id="tdata"><b>CCA</b></td>
                      <td id="tdata"><b>SPA</b></td>
                      <td id="tdata"><b>COYA</b></td>
                      <td id="tdata"><b>HTA</b></td>
                      <td id="tdata"><b>GPF</b></td>
                      
                   </tr>
                      
                    <tr id='grossinput'>
                        <td><input type="text" name="cca" class="cca" id="cca"/></td>
                        <td><input type="text" name="ca" id="ca" class="ca"/></td>
                        <td><input type="text" name="cona" class="cona" id="cona"/></td> 
                        <td><input type="text" name="hta" class="hta" id="hta"/></td>
                    <td><input type="text" name="gp" id="gp" class="gp"/></td>
                    
                    </tr>
                    <tr>
                        <td id="tdata"><b>SLI</b></td>
                        <td id="tdata"><b>LIC</b></td>
                        <td id="tdata"><b>HBA</b></td>
                        <td id="tdata"><b>FBS</b></td>
                         <td id="tdata"><b>GIS</b></td>
                      
                     </tr>
                    <tr id='grossinput'>
                     <td><input type="text" name="sli" class="sli" id="sli" /></td>
                     <td><input type="text" name="lic" class="lic" id="lic"/></td>   
                     <td><input type="text" name="hba" class="hba" id="hba"/></td>
                     <td><input type="text" name="fbs"class="fbs" id="fbs"/></td>
                    <td><input type="text" name="gis" class="gis" id="gis"/></td>
                    </tr>
                    <tr>
                        <td id="tdata"><b>HBA Interest</b></td>
                        <td id="tdata"><b>IncomeTax</b></td>
                        <td id="tdata"><b>PLI</b></td>
                        <td id="tdata"><b>GPAIS</b></td>
                    </tr>
                    <tr id='grossinput'>
                    <td><input type="text" name="inter" class="inter" id="inter"/></td>    
                    <td><input type="text" name="tax" class="tax" id="tax"/></td>    
                    <td><input type="text" name="pli" class="pli" id="pli"/></td>
                    <td><input type="text" class="gpais" id="gpais" name="gpais"/></td>
                    <td><input type="hidden" name="gross" class="gross" id="gross"/></td>   
                    <td><input type="hidden" name="deduct" class="deduct" id="deduct"/></td> 
                    <td><input type="hidden" name="net" class="net" id="net"/></td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                        <td><input type="button" id='submit' class="submit" value='Submit'>
                        <input type="reset" id='clear' value='Clear' class=""></td>
                    </tr>
                </table>  
            <table id="append_tab" class="append_tab">
                <tr id="append0" class="append0">
                    <td>Month</td>
                    <td>BP</td>
                    <td>Class</td>
                    <td>DA</td>
                    <td>HRA</td>
                    <td>CCA</td>
                    <td>SPA</td>
                    <td>COYA</td>
                    <td>HTA</td>
                    <td>Gross</td>
                    <td>GPF</td>
                    <td>SLI</td>
                    <td>LIC</td>
                    <td>HBA</td>
                    <td>FBS</td>
                    <td>GIS</td>
                    <td>Interest</td>
                    <td>Tax</td>
                    <td>PLI</td>
                    <td>GPAIS</td>
                    <td>deduct</td>
                    <td>Net</td>
                    
                    
               </tr>
            </table>
          </div>
<script>
//$(document).ready(function()
//{

$(".bp").blur(function()
{ 
 val=$(".bp").val();
   if(val)
            {
                month=$(".month").val();
                if(month<4)
                     m=1;
                 else if(month>=4 && month<11)
                     m=2;
                 else if(month>=11)
                     m=3;
               
               if(m==1)
                 da=(val*45)/100;
             else if(m==2)
                 da=(val*53)/100;
              else if(m==3)
                 da=(val*63)/100;
             
                 $(".da").val(Math.round(da));
            }
                 

   });

$( document ).ready(function() {
    
    

    $(".city").change(function(){
        val=$(".bp").val();
       // alert(val)
       if(val)
           {
              city=$(".city").val();   
              if(val>=8500 && val<=8729)
                  {
                      if(city==1)
                          $(".hra").val(350);
                      else if(city==2)
                         $(".hra").val(270);
                      else if(city==3)
                          $(".hra").val(270);
                      else
                          $(".hra").val(250);
                  }
                  else if(val>=8730 && val<=12549)
                  {
                      if(city==1)
                          $(".hra").val(560);
                      else if(city==2)
                          $(".hra").val(390);
                      else if(city==3)
                          $(".hra").val(390);
                      else
                          $(".hra").val(250);
                  }
                  else if(val>=12550 && val<=24039)
                  {
                      if(city==1)
                          $(".hra").val(840);
                      else if(city==2)
                          $(".hra").val(550);
                      else if(city==3)
                          $(".hra").val(480);
                      else
                          $(".hra").val(250);
                  }
                  else if(val>=24040 && val<=29179)
                  {
                      if(city==1)
                          $(".hra").val(1050);
                      else if(city==2)
                          $(".hra").val(700);
                      else if(city==3)
                          $(".hra").val(530);
                      else
                          $(".hra").val(250);
                  }
                  else if(val>=29180 && val<=33679)
                  {
                      if(city==1)
                          $(".hra").val(1400);
                      else if(city==2)
                          $(".hra").val(950);
                      else if(city==3)
                          $(".hra").val(530);
                      else
                          $(".hra").val(250);
                  }
                  else 
                  {
                      if(city==1)
                          $(".hra").val(1680);
                      else if(city==2)
                          $(".hra").val(1110);
                      else if(city==3)
                          $(".hra").val(530);
                      else
                          $(".hra").val(250);
                  }
//                   cca(val);
           bp=val;
        city=$(".city").val(); 
                if(city==1) 
                    {
                  if(bp>=8500 && bp<=9439)
                       $(".cca").val(200);
                  else if(bp>=9440 && bp<=13539)
                       $(".cca").val(250);
                  else if(bp>=13540 && bp<=16979)
                       $(".cca").val(300);
                   else 
                       $(".cca").val(350);
                    }
                 else
                     $(".cca").val(0);
            }
    });
   
});
var i=0;
//function cca(val)
//    { 
//       
//              
//           
//    }
        $('.hta').change(function()
  {
      
    bp=$(".bp").val();
    da=$(".da").val();
         hra=$(".hra").val();
         cca=$(".cca").val();
         ca=$(".ca").val();
         cona=$(".cona").val();
         hta=$(".hta").val();
         gr=parseInt(bp)+parseInt(da)+parseInt(hra)+parseInt(cca)+parseInt(ca)+parseInt(cona)+parseInt(hta);
          $(".gross").val(gr);
   });
   $('.gpais').change(function()
  {
         gp=$(".gp").val();
         sli=$(".sli").val();
         lic=$(".lic").val();
         hba=$(".hba").val();
         fbs=$(".fbs").val();
         gis=$(".gis").val();
         inter=$(".inter").val();
         tax=$(".tax").val();
         pli=$(".pli").val();
         gpais=$(".gpais").val();
         ded=parseInt(gp)+parseInt(sli)+parseInt(lic)+parseInt(hba)+parseInt(fbs)+parseInt(gis)+parseInt(inter)+parseInt(tax)+parseInt(pli)+parseInt(gpais);
         $(".deduct").val(ded);
         gr=$(".gross").val();
         net=parseInt(gr)-parseInt(ded);
         $(".net").val(net);       
//        alert(ne);
  });
     $("#submit").click(function()
     {val=$(".bp").val();
   if(val)
            {
                month=$(".month").val();
                if(month<4)
                     m=1;
                 else if(month>=4 && month<11)
                     m=2;
                 else if(month>=11)
                     m=3;
               
               if(m==1)
                 da=(val*45)/100;
             else if(m==2)
                 da=(val*53)/100;
              else if(m==3)
                 da=(val*63)/100;
             
                 $(".da").val(Math.round(da));
            }
       
        
         bp=$(".bp").val();
         if(!bp) bp=0;
         month=$(".month").val();
         if(!month) month=0;
         city=$(".city").val();
         if(!city) city=0;
         da=$(".da").val();
         if(!da) da=0;
         hra=$(".hra").val();
         if(!hra) hra=0;
         cca=$(".cca").val();
         if(!cca) cca=0;
         ca=$(".ca").val();
         if(!ca) ca=0;
         cona=$(".cona").val();
         if(!cona) cona=0;
         hta=$(".hta").val();
         if(!hta) hta=0;
         gross=parseInt(bp)+parseInt(da)+parseInt(hra)+parseInt(cca)+parseInt(ca)+parseInt(cona)+parseInt(hta);
         if(!gross) gross=0;$("#gross").val(gross);
         gp=$("#gp").val();
         if(!gp) gp=0;
         sli=$("#sli").val();
         if(!sli) sli=0;
         lic=$("#lic").val();
         if(!lic) lic=0;
         hba=$("#hba").val();
         if(!hba) hba=0;
         fbs=$("#fbs").val();
         if(!fbs) fbs=0;
         gis=$("#gis").val();
         if(!gis) gis=0;
         inter=$("#inter").val();
         if(!inter) inter=0;
         tax=$("#tax").val();
         if(!tax) tax=0;
         pli=$("#pli").val();
         if(!pli) pli=0;
         gpais=$("#gpais").val();
         if(!gpais) gpais=0;
         deduct=parseInt(gp)+parseInt(sli)+parseInt(lic)+parseInt(hba)+parseInt(fbs)+parseInt(gis)+parseInt(inter)+parseInt(tax)+parseInt(pli)+parseInt(gpais);

        if(!deduct) deduct=0; $("#deduct").val(deduct);
         net=parseInt(gross)-parseInt(deduct);////$("#bp").val()+("#da").val()+$("#hra").val()+$("#cca").val()+$("#ca").val();
         if(!net) net=0;  $("#net").val(net);
         
         
         
     url='<?php echo base_url();?>index.php/control/insert';
     datastring=$('#frminsert').serialize();
     $.ajax({
        type:"POST",
        data:datastring,
        url:url,
        success:function (data){
            
        
           if(data=='sucess'){
             j=i+1; 
             $('#month').val(i+2);
//             alert(i);
 mon='';m=parseInt(month);
        switch(m)
        {
            case 1:
                mon="March";
                break;
            case 2:
                mon="April";
                break;
            case 3:
                mon="May";
                break;
             case 4:
                mon="June";
                break;
            case 5:
                mon="July";
                break;
            case 6:
                mon="August";
                break;
             case 7:
                mon="September";
                break;
            case 8:
                mon="October";
                break;
            case 9:
                mon="November";
                break;
             case 10:
                mon='December';
                break;
            case 11:
                mon="January";
                break;
            case 12:
                mon="February";
                break;    
            
        }
                 $("#append"+i).after("<tr id='append"+j+"'><td><input type='hidden' name='month"+i+"' value='"+mon+"'>"+mon+"</td><td><input type='hidden' name='bp"+i+"' value='"+bp+"'>"+bp+"</td><td><input type='hidden' name='city"+i+"' value='"+city+"'>"+city+"</td><td><input type='hidden' name='da"+i+"' value='"+da+"'>"+da+"</td><td><input type='hidden' name='hra"+i+"' value='"+hra+"'>"+hra+"</td><td><input type='hidden' name='cca"+i+"' value='"+cca+"'>"+cca+"</td><td><input type='hidden' name='ca"+i+"' value='"+ca+"'>"+ca+"</td><td><input type='hidden' name='cona"+i+"' value='"+$(".cona").val()+"'>"+$(".cona").val()+"</td><td><input type='hidden' name='hta"+i+"' value='"+hta+"'>"+hta+"</td><td><input type='hidden' name='gross"+i+"' value='"+gross+"'>"+gross+"</td><td><input type='hidden' name='gp"+i+"' value='"+gp+"'>"+gp+"</td><td><input type='hidden' name='sli"+i+"' value='"+sli+"'>"+sli+"</td><td><input type='hidden' name='lic"+i+"' value='"+lic+"'>"+lic+"</td><td><input type='hidden' name='hba"+i+"' value='"+hba+"'>"+hba+"</td><td><input type='hidden' name='fbs"+i+"' value='"+fbs+"'>"+fbs+"</td><td><input type='hidden' name='gis"+i+"' value='"+gis+"'>"+gis+"</td><td><input type='hidden' name='inter"+i+"' value='"+inter+"'>"+inter+"</td><td><input type='hidden' name='tax"+i+"' value='"+tax+"'>"+tax+"</td><td><input type='hidden' name='pli"+i+"' value='"+pli+"'>"+pli+"</td><td><input type='hidden' name='gpais"+i+"' value='"+gpais+"'>"+gpais+"</td><td><input type='hidden' name='deduct"+i+"' value='"+deduct+"'>"+deduct+"</td><td><input type='hidden' class='net"+i+"' name='net"+i+"' value='"+net+"'>"+net+"</td></tr>");
                 i++;
                 alert("Inserted Successfully");
             }else
                 alert('data alredy exist');
                 
        }
     });
        if(i>=11){
        
        <?php
          $this->session->set_userdata(array('session_pan'=>32500));
          ?>
            window.location="<?php echo base_url().'index.php/control/second';?>";
        }
     });
//  $('#ca').change(function()
//  {
//      
//    bp=$("#bp").val();
//    da=$("#da").val();
//         hra=$("#hra").val();
//         cca=$("#cca").val();
//         ca=$("#ca").val();
//         gr=parseInt(bp)+parseInt(da)+parseInt(hra)+parseInt(cca)+parseInt(ca);
//          $(".gross").val(gr);
//   });

  
  $('#year').change(function() 
  { 
  var pan=$('#pan').val();
  var year=$('#year').val();
  url='<?php echo base_url().'index.php/control/get_details';?>';
	
    $.ajax({     
     type: "POST",
     url: url,
     dataType:'json',
     data:{'panval':pan,'yearval':year},
     
     success: function (data) {
        i=data.i; 
        $('#month').val(i+1);
        $("#append0").after(data.row);
        
     }
         
//         j=i+1; 
//         $("#append"+i).after("<tr id='append"+j+"'><td><input type='hidden' name='month"+i+"' value='"+month+"'>"+month+"</td><td><input type='hidden' name='bp"+i+"' value='"+bp+"'>"+bp+"</td><td><input type='hidden' name='city"+i+"' value='"+city+"'>"+city+"</td><td><input type='hidden' name='da"+i+"' value='"+da+"'>"+da+"</td><td><input type='hidden' name='hra"+i+"' value='"+hra+"'>"+hra+"</td><td><input type='hidden' name='cca"+i+"' value='"+cca+"'>"+cca+"</td><td><input type='hidden' name='ca"+i+"' value='"+ca+"'>"+ca+"</td><td><input type='hidden' name='gp"+i+"' value='"+gp+"'>"+gp+"</td><td><input type='hidden' name='sli"+i+"' value='"+sli+"'>"+sli+"</td><td><input type='hidden' name='lic"+i+"' value='"+lic+"'>"+lic+"</td><td><input type='hidden' name='hba"+i+"' value='"+hba+"'>"+hba+"</td><td><input type='hidden' name='fbs"+i+"' value='"+fbs+"'>"+fbs+"</td><td><input type='hidden' name='gis"+i+"' value='"+gis+"'>"+gis+"</td><td><input type='hidden' name='inter"+i+"' value='"+inter+"'>"+inter+"</td><td><input type='hidden' name='tax"+i+"' value='"+tax+"'>"+tax+"</td></tr>");
//         i++;
     
                 
     })
     
//     if(i>11){
//         var a=1;
//          $('#myanchor').click();
//           location.href="<?php echo base_url()?>index.php/control/second/"+a; 
//        }
   })
 </script> 
 <a href="<?php echo base_url()?>index.php/control/second/">Next Page</a>
    </form>

    
        
        

