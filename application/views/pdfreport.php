<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "";

$obj_pdf->SetDefaultMonospacedFont('helvetica');

$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('times', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->setPrintHeader(false);
$obj_pdf->setPrintFooter(false);

$obj_pdf->AddPage();
ob_start();

    // we can have any view part here like HTML, PHP etc

foreach($deatil as $yy)
 { 
   $pan=$yy->pan;
   $year=$yy->year;
 }
$year0=$year-0;
 $year1=$year+1;
 $year2=$year1+1;
$deatils3=$deatil1;
foreach($deatil2 as $rr)
 {
$depo=$rr->deposit;
  $med=$rr->mediclaim;   
  $tr=$rr->treat;
  $inc=$rr->incur;
  $lon=$rr->loan;
  $dle=$rr->disable;
  $nsc1=$rr->nsc;
  $rel=$rr->releif;
  $unit=$rr->unit;
  $plan=$rr->plan;
  $mutual=$rr->mutual;
  $scheme=$rr->scheme;
  $fee=$rr->fee;
  $house=$rr->house;
  $eligible=$rr->eligible;
  $fund=$rr->fund;
  }

foreach($deatil1 as $rr1)
 {
     $oth=$rr1->other;
 }
foreach($deatil3 as $rr2) 
{
 $name=$rr2->employee_name;
 $emplr_name=$rr2->employer_name;
 $address=$rr2->address;
 $desig=$rr2->designation;
 $tan=$rr2->tan;
 }

$html = '<div align="center"><div align="right"><i><b>P A N NO. '.$pan.'</b></i></div>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>INCOME TAX STATEMENT FOR THE FINANCIAL YEAR '.$year.'</b><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<b>Assessment Year '.$year1.'-'.$year2.'</b>)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>In respect of Shri/Smt &nbsp;&nbsp;'.$name.'</b><br/>
(To be furnished by the employees/Officers whose Gross Total Income exceed Rs.2,00,000/-)
</div>';
$html .= '

  <table border="1" cellpadding="2" style="padding-left:40px">
    <tr>
      <td style="width:30px">1 (a)</td>
      <td colspan="8">Gross Salary Income(includes Salary D A, H R A, C C A, Interim Relief ,O T Allowance Deputation Allowance ,Medical Allowance etc:)</td>
    </tr>';
$total=0;$hratotal=0;$salary=0;$conatotal=0;$hbatotal=0;$lictotal=0;$gpftotal=0;$slitotal=0;$gistotal=0;$fbstotal=0;$taxtotal=0;$htatotal=0;$plitotal=0;$gpaistotal=0;
       foreach($deatil as $row)
            {
        $mon='';
        switch($row->month)
        {
            case 1:
                $mon='March';
                break;
            case 2:
                $mon='April';
                break;
            case 3:
                $mon='May';
                break;
             case 4:
                $mon='June';
                break;
            case 5:
                $mon='July';
                break;
            case 6:
                $mon='August';
                break;
             case 7:
                $mon='September';
                break;
            case 8:
                $mon='October';
                break;
            case 9:
                $mon='November';
                break;
             case 10:
                $mon='December';
                break;
            case 11:
                $mon='January';
                break;
            case 12:
                $mon='February';
                break;    
            
        }
       $total=$total+$row->gross;
       $hratotal=$hratotal+$row->hra;
       $salary=$salary+$row->bp+$row->da;
       $conatotal=$conatotal+$row->cona;
        $hbatotal=$hbatotal+$row->Interest;
       $lictotal=$lictotal+$row->lic;
      $gpftotal=$gpftotal+$row->gpf;
       $slitotal=$slitotal+$row->sli;
       $gistotal=$gistotal+$row->gis;
       $fbstotal=$fbstotal+$row->fbs;
       $taxtotal=$taxtotal+$row->tax;
       $plitotal=$plitotal+$row->pli;
       $htatotal=$htatotal+$row->hta;
       $gpaistotal=$gpaistotal+$row->gpais;
    $html .= '<tr>
      <td>&nbsp;</td>
      <td align="left">'.$mon.'</td>';
if($row->month<=10)
      $html .= '<td  align="center">'.$year0.'</td>';
else
	 $html .= '<td  align="center">'.$year1.'</td>';
      $html .= '<td colspan="5">&nbsp;</td>
      <td  align="right">'.$row->gross.'</td>
    </tr>';
    }
   foreach($deatils3 as $row)
    { 
    $t_income=$total+($row->leave)+($row->fest_allo)+($row->paygpf)+($row->daarrier)+($row->daarrier1)+($row->medical)+($row->paycash);
    $act_rent_paid=($row->rent)-($salary/10);
    $proftax=($row->profe1)+($row->profe2);
    $perc_salary=($salary*40)/100;
    $total_hba=$hbatotal+($row->interhba);
    $least=0;$balance=0;$less=0;
    $booklic=$row->licbook;
    $togpf=$row->paygpf;
    $tocash=$row->paycash;
    $pay=$togpf+$tocash;
    $arrier=$row->daarrier;
     $arrier1=$row->daarrier1;
    $arriertotal=$arrier+$arrier1;
    if($hratotal<$act_rent_paid)
    $least=$hratotal;
    elseif($act_rent_paid<$perc_salary)
    $least=$act_rent_paid;
    else 
    $least=$perc_salary; 
    $less=$least;
    $less=round($less);
    if($less<0)
    {
        $balance=$t_income;
        $less=0;
    }
    else
        $balance=$t_income-$less;
    $netsalary=$balance-($conatotal+$proftax);
    $grossincome=$netsalary-($total_hba+($row->other));
    
   $html .= ' <tr>
      <td align="right">(b)</td>
      <td colspan="7"><b>Leave Surrender.</b></td>
      <td align="right">'.$row->leave.'</td>
    </tr>
    <tr>
      <td align="right">(c)</td>
      <td colspan="7"><b>Festival Allowance/Bonus/Ex-gratia and Incentive.</b></td>
      <td align="right">'.$row->fest_allo.'</td>
    </tr>
    <tr>
            <td align="right">(d)</td>         
            <td colspan="3">Pay Revision Arrears/Arrear Salary</td>
            <td align="center"><strong>To GPF</strong></td>
            <td align="right">'.$row->paygpf.'</td>
            <td align="center"><strong>In Cash</strong></td>
            <td align="right">'.$row->paycash.'</td>
            <td align="right">'. $pay.'</td>
   </tr>
   <tr>
      <td align="right">(e)</td>
      <td colspan="7">DA Arrears******</td>
      <td align="right">'.$arriertotal.'</td>
    </tr>
   <tr>
      <td align="right">(f)</td>
      <td colspan="7">Medical  reimbursment in excess of Rs 15,000/-</td>
      <td align="right">'.$row->medical.'</td>
    </tr> 
    <tr>
      <td align="right">(g)</td>
      <td colspan="7"><b>Total Salary Income(a + b + c + d +e+ f)</b></td>
      <td align="right">'.$t_income.'</td>
    </tr> 
    <tr>
      <td>2</td>
      <td colspan="7">Deduct: H R A in the case of persons who actually incur expenditure by way of rent.</td>
      <td align="right">'.$less.'</td>
    </tr>
    <tr>
            <td></td>
            <td colspan="6">i) Actual H R A received during the year.</td>         
            <td align="right">'.$hratotal.'</td>
            <td rowspan="4"></td>
    </tr>
    <tr>
            <td></td>
            <td colspan="6">ii) Actual rent paid excess of 1/10th of the salary.</td>         
            <td colspan="1" align="right">'.$act_rent_paid.'</td>
           
    </tr>
    <tr>
            <td></td>
            <td colspan="6">iii) 40% of the salary.</td>         
            <td colspan="1" align="right">'.$perc_salary.'</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="7">(i) to (iii) whichever is least is exempted.</td>   
    </tr>
  <tr>
      <td>3</td>
      <td colspan="7"><strong>Balance(1-2)</strong></td>
      <td align="right">'.$balance.'</td>
    </tr>
    <tr>
      <td>4</td>
      <td colspan="7"><strong>Deduct:</strong></td>
      <td align="right"></td>
    </tr>
    <tr>
      <td align="right">(a)</td>
      <td colspan="7">Conveyance Allowance to Handycapped Rs.1000/- per month.</td>
      <td align="right">'. $conatotal.'</td>
    </tr>
    <tr>
      <td align="right">(b)</td>
      <td colspan="7"><strong>Profession Tax Paid.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Section 16(iii))</strong></td>
      <td align="right">'.$proftax.'</td>
    </tr>
    <tr>
      <td>5</td>
      <td colspan="7"><strong>Net salary Income(3-4)</strong></td>
      <td align="right">'.$netsalary.'</td>
    </tr>
     <tr>
      <td>6</td>
      <td colspan="7"><strong>Deduct Interest/Accured interest on H B A</strong>(maximum admissible amount is rs.30,000/-if the property is acquired or constructed on or after 01.04.1999 and such acquisition or construction is completed within three years of from the end of the financial year in which capital is borrowed deductible amount is Rs.1.50 Lakh)<b>18EE</b></td>
      <td align="right">'.$total_hba.'</td>
    </tr>
    <tr>
      <td>7</td>
      <td colspan="7">Any other Income(Business,Capital Gains or other Sources).</td>
      <td align="right">'.$row->other.'</td>
    </tr>
    <tr>
      <td>8</td>
      <td colspan="7"><strong>Gross Total Income (5-6+7)</strong></td>
      <td align="right">'.$gross6000income.'</td>
    </tr>
    <tr>
      <td>9</td>
      <td colspan="7"><strong>Deduct:</strong></td>
      <td align="right"></td>
    </tr>';
    }

foreach($deatil2 as $row)
            { 
     $med=$row->mediclaim;
       if(($row->mediclaim)>20000)
           $med=20000;
     $html .= '<tr>
      <td align="right">(a)</td>
      <td colspan="7">Mediclaim (maximum of Rs.15,000)- taken on the health of the tax payer,spouse,dependent parent or depent children and if it is taken on any person who is senior citizen Rs 20,000/-)&nbsp;&nbsp;&nbsp;&nbsp;<b>(Section 18 D)</b></td>
      <td align="right">'.$med.'</td>
    </tr>
    <tr>
      <td align="right">(b)</td>
      <td colspan="7">Expenditure on medical treatment of mentally or physically handicapped dependents(including the amount deposited in their name [max.Rs.50,000/-in case of treatment is made to a person who is senior citizen Rs.1,00,000/-].&nbsp;&nbsp;&nbsp;&nbsp;<b>(Section 80DD)</b></td>
      <td align="right">'.$row->treat.'</td>
    </tr>
    <tr>
      <td align="right">(c)</td>
      <td colspan="7">Expenditure incurred on medical treatment of the employee for specified deceases or ailment like cancer,AIDS,etc. [max.Rs.40,000/- in case of treatment is made to a person who is senior citizen Rs.60,000/-].&nbsp;&nbsp;&nbsp;&nbsp;<b>(Section 80DDB)</b></td>
      <td align="right">'.$row->incur.'</td>
    </tr>
    <tr>
      <td align="right">(d)</td>
      <td colspan="7">Interest paid on loan taken for higher education of self or relative (max of Rs.40,000/-).&nbsp;&nbsp;&nbsp;&nbsp;<b>(Section 80E)</b></td>
      <td align="right">'.$row->loan.'</td>
    </tr>
    <tr>
      <td align="right">(e)</td>
      <td colspan="7">Employees with Disability a deduction of Rs.50,000/- (40% to 80%) Severe Disability,deduction will be Rs.1,00,000/-(Above80%)&nbsp;&nbsp;&nbsp;&nbsp;<b>(Section 80U)</b></td>
      <td align="right">'.$row->disable.'</td>
    </tr>
    </table>
    <br/>
    <br/>
    </div>';

$obj_pdf->writeHTML($html, true, false, true, false, '');




    
    
    $totaldeduct=($row->mediclaim)+($row->treat)+($row->incur)+($row->loan)+($row->disable);
    $deduct=$grossincome- $totaldeduct;
    $licdeduct=$lictotal+($booklic);
    $gpfdeduct=$gpftotal+$togpf+$arrier+$arrier1;
    $totalsection=0;$totalincome=0;
    $totalsection=$totalsection+$licdeduct+$gpfdeduct+$slitotal+$gistotal+$fbstotal+$gpaistotal+($row->nsc)+($row->deposit)+($row->unit)+($row->plan)+($row->mutual)+($row->scheme)+($row->fee)+($row->house)+($row->eligible)+($row->fund);
    $totalsection1=$totalsection;
    if($totalsection1>100000)
            $totalsection1=100000;
    $totalincome=$deduct-$totalsection1;
     $totincome=$totalincome;
     $rem=$totincome%10;
     $rebate=0;$exce=0;
    if($rem<5)
     $totincome=$totincome-$rem;
    else
      $totincome=$totincome+(10-$rem);
    if($totincome<=200000)
        $excess="nil";$excess="nil"; $excess1="nil"; $excess2="nil";
    if($totincome>=200000 && $totincome<=500000)
       {
        $excess=(($totincome-200000)*10)/100;
        $exce=$excess;$rebate=2000;
        
       }
       else if($totincome>500000 && $totincome<=1000000)
       {
           $excess1=((($totincome-500000)*20)/100)+30000;
           $exce=$excess1;$rebate=0;
       }
       else if($totincome>1000000)
       {
           $excess2=((($totincome-1000000)*30)/100)+130000;
           $exce=$excess2;$rebate=0;
       }
       $surcharge=0;
       if($totincome>10000000)
       {
           $surcharge=((($totincome)*10)/100);
       }
       else
            $surcharge=0;
       $sur=$surcharge+$exce;
       $cess=($sur*3)/100;
       $roundcess=round($cess);
        if($totincome>=200000 && $totincome<=500000)
       {
           $sur=$excess-$rebate;
           $cess=($sur*3)/100;
           $roundcess=round($cess);
           $totaltax=$cess+$sur;
       }
       $totaltax=$roundcess+$sur;
       $roundtotaltax=round($totaltax);
       $bal=$roundtotaltax-($row->releif);
      // $bal=$bal-$rebate;
       if($bal<=0)
           $bal='Nil';
       $tobepaid=$bal-$taxtotal;
      
       if($rebate>=$excess && $rebate>0)
       {    $rebate=$excess;
            $roundcess='Nil';
            $bal='Nil';
            $totaltax='Nil';
             $sur='Nil';
        }
  $html ='<div> 
  <table width="100%" border="1" cellpadding="3">     
      <tr>
      <td style="width:30px" >10</td>
      <td colspan="7"><strong>Deduct(8-9):</strong></td>
      <td align="right">'.$deduct.'</td>
    </tr>
    <tr>
      <td>11</td>
      <td colspan="7"><strong>Deduction under section 80 c(Maximum Rs. 1,00,000/-)</strong></td>
      <td align="right">'.$totalsection1.'</td>
    </tr>
    <tr>
           <td align="right">(a)</td>
            <td colspan="6"><strong>Life Insurance Premia </strong>of Self,Spouse &amp; Children.</td>         
            <td align="right">'.$licdeduct.'</td>
            <td rowspan="16"></td>
    </tr>
    <tr>
           <td align="right">(b)</td>
            <td colspan="6">Purchase of N S C VIII issue.</td>         
            <td colspan="1" align="right" >'.$row->nsc.'</td>
    </tr>
    <tr>
           <td align="right">(c)</td>
            <td colspan="6">Contribution to <strong>GPF/Pay fixation arr:D A arr:</strong></td>         
            <td colspan="1" align="right">'.$gpfdeduct.'</td>
    </tr>
   <tr>
           <td align="right">(d)</td>
            <td colspan="6">Contribution to <strong>S L I</strong></td>         
            <td colspan="1" align="right">'.$slitotal.'</td>
    </tr> 
    <tr>
           <td align="right">(e)</td>
            <td colspan="6">Contribution to <strong>G I S</strong></td>         
            <td colspan="1" align="right">'.$gistotal.'</td>
    </tr>
    
    <tr>
           <td align="right">(f)</td>
            <td colspan="6">Contribution to <strong>F B S</strong></td>         
            <td colspan="1" align="right">'.$fbstotal.'</td>
    </tr>
    <tr>
           <td align="right">(g)</td>
            <td colspan="6">Team deposit with scheduled bank for a fixed period of not less than 5 years.</td>         
            <td colspan="1" align="right">'.$row->deposit.'</td>
    </tr>
    <tr>
           <td align="right">(h)</td>
            <td colspan="6">Contribution towards Unit Linked Insurance Plan of U T I or L I C (of self,Spouse &amp; Children)</td>         
            <td colspan="1" align="right">'.$row->unit.'</td>
    </tr>
     <tr>
           <td align="right">(i)</td>
            <td colspan="6">Payment under a contract for annually plan of the L I C or any other insurer</td>         
            <td colspan="1" align="right">'.$row->plan.'</td>
    </tr>
     <tr>
           <td align="right">(j)</td>
            <td colspan="6">Purchase of tax saving units of Mutual Fund or U T I</td>         
            <td colspan="1" align="right">'.$row->mutual.'</td>
    </tr>
    <tr>
           <td align="right">(k)</td>
            <td colspan="6">Contribution to any Deposit Scheme or pension fund set up by National Housing Bank </td>         
            <td colspan="1" align="right">'.$row->scheme.'</td>
    </tr>
    <tr>
           <td align="right">(l)</td>
            <td colspan="6">Tution fee(paid to university,college,school or educational institution situated within India for full-time education to any 2 children</td>         
            <td colspan="1" align="right">'.$row->fee.'</td>
    </tr>
    <tr>
           <td align="right">(m)</td>
            <td colspan="6">Housing Loan Repayment(Principal)&amp; Stamp duty paid for purchase of property</td>         
            <td colspan="1" align="right">'.$row->house.'</td>
    </tr>
    <tr>
           <td align="right" >(n)</td>
            <td colspan="6">Subscription to equity shares of debentures of an eligible issue</td>         
            <td colspan="1" align="right">'.$row->eligible.'</td>
    </tr>
   <tr>
           <td align="right">(o)</td>
            <td colspan="6">Subscription to eligible units of mutual fund.</td>         
            <td colspan="1" align="right">'.$row->fund.'</td>
    </tr>
    <tr>
           <td align="right">(o)</td>
            <td colspan="6">Contribution to GPAIS.</td>         
            <td colspan="1" align="right">'.$gpaistotal.'</td>
    </tr>
    <tr>
            <td>12</td>
            <td colspan="6">Total.</td>         
            <td colspan="1" align="right">'.$totalsection1.'</td>
    </tr>
    <tr>
           <td>13</td>
            <td colspan="7"><strong>Tax on Total Income</strong></td>        
            <td align="right">'.$totalincome.'</td>
    </tr>
    <tr>
           <td>14</td>
            <td colspan="7"><strong>Total income rounded off to nearest multiple of ten Rupees(9-10)</strong></td>        
            <td align="right">'.$totincome.'</td>
    </tr> 
    <tr>
      <td></td>
      <td colspan="7"><u><strong>FOR MEN &nbsp; WOMEN</strong></u><strong>.(1)</strong>Total Income up to Rs.2 Lakhs-Nil.</td>
      <td align="right"></td>
    </tr>
    <tr>
      <td></td>
      <td colspan="7"><strong>(2)</strong>Total Income above Rs.2 lakhs up to 5 lakhs-10% of total Income in excess of Rs 2,00,001 lakhs. </td>
      <td align="right">'.$excess.'</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="7"><b>Less:Rebate</b></td>
      <td align="right">'.$rebate.'</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="7"><strong>(3)</strong>Total Income above Rs.5,00,001 lakhs up to 10 lakhs Rs.30,000 plus 20% of total income in excess of Rs.5,00,001/-lakhs.</td>
      <td align="right">'.$excess1.'</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="7"><strong>(4)</strong>Total Income exceeds Rs.10,00,001 lakhs-Rs.10,00,001 lakhs-Rs.1,30,000/- plus 30% of Total Income in excess of Rs.10,00,001/-lakhs.</td>
      <td align="right">'.$excess2.'</td>
    </tr>
    <tr>
      <td>15</td>
      <td colspan="7">Add:Surcharge @10% if the total income exceeds Rs.10,00,000/-</td>
      <td align="right">'.$surcharge.'</td>
    </tr>
    <tr>
      <td>16</td>
      <td colspan="7"><strong>Income Tax &amp;&Surcharge Payable(14+15).</strong></td>
      <td align="right">'.$sur.'</td>
    </tr>
    <tr>
      <td>17</td>
      <td colspan="7"><strong>Education cess[@ 3 % of (16)].</strong></td>
      <td align="right">'.$roundcess.'</td>
    </tr>
    <tr>
    <td>18</td>
    <td colspan="7"><strong>Total Tax Payable(16+17).</strong></td>
    <td align="right">'. $roundtotaltax.'</td>
  </tr>
  <tr>
    <td>19</td>
    <td colspan="7">Less:Relief for arrears of salary U/S.89(1).</td>
    <td align="right">'.$row->releif.'</td>
        </tr>';
  }
 $html.='<tr>
    <td>20</td>
    <td colspan="7"><b>Balance Tax Payable</b></td>
    <td align="right">'.$bal.'</td>
  </tr>
  <tr>
    <td>21</td>
    <td colspan="7">Amount of Tax already deducted from Salary.</td>
    <td align="right">'.$taxtotal.'</td>
  </tr>
  <tr>
    <td>22</td>
    <td colspan="7"><b>Balance Income Tax to be paid.</b></td>
    <td align="right">'.$tobepaid.'</td>
  </tr>
    </table>
<table width="100%" border="0">
<tr><td colspan="5">Place:Thiruvananthapuram,</td>
     <td colspan="4" align="center">Signature.</td>
 </tr>
 <tr><td colspan="5">Date:----------------</td>
     <td colspan="4" style="float:right;padding-right:100px;text-align:center;">(Name,Designation & Office)</td>
 </tr>
<tr><td colspan="9" align="center">DECLARATION</td></tr>
<tr><td colspan="9" align="center">(Cases in which the amount of H R A drawn is excluded from the Gross salary)</td></tr>
<tr><td colspan="9">I.................................................do here by declare that I am actually incurring
 expenditure towards payment of rent of my residential accomodation to HouseNo.......................................Place..............................................and that the amount of rent actually paid by me during
 .....................................is Rs................................................</td></tr>
 <tr><td colspan="5">Place:Thiruvananthapuram,</td>
     <td colspan="4" align="center">Signature.</td>
 </tr>
 <tr><td colspan="5">Date:----------------</td>
     <td colspan="4" style="float:right;padding-right:100px;text-align:center;">(Name,Designation & Office)</td>
 </tr>
 </table>
 </div>';
$obj_pdf->writeHTML($html, true, false, true, false, '');

// create some HTML content
$html = '<div align="center"><b>FORM No.16</b><br/>
         [See Rule 31(1)(a)]<br/>
<b>CERTIFICATE UNDER SECTION 203 OF THE INCOME TAX ACT,1961 FOR TAX<br/>DEDUCTED AT SOURCE FROM INCOME CHARGEABLE UNDER<br/>THE HEAD "SALARIES"</b>
</div>';

$html .= '    
<div>
  <table width="100%" border="1" cellpadding="0" >
    <tr><td>&nbsp;Name and Address of the Employer<br/>
'.$emplr_name.'<br/>
   '.$address.'<br/>
    <br/>
    </td>
    <td>
    &nbsp;Name and Designation of the Employee<br/>
    '.$name.'<br/>
    '.$desig.'<br/>
    <br/>
    </td></tr>
    <tr>
    <td valign="top">&nbsp;PAN/GIR No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$pan.'</td>
    <td>TAN No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$tan.'
    </td>
    </tr>
  <tr>
     <td>&nbsp;TDS Circle where Annual Return  /Statement under section 206 is to be filled</td>
     <td rowspan="2"><table height="100%" width="100%" border="1" cellpadding="0">
                     
                      <tr height="60">
                          <td colspan="2" width="50%">
                              <table height="100%" width="100%" cellpadding="2">
                                  <tr><td colspan="2" align="center">Period</td></tr>
                                  <tr><td  align="center">From</td><td  align="center">To</td></tr>
                              </table>
                          </td>
                          <td valign="top" rowspan="2" width="50%" align="center">Assessment Year.......<br>'.$year1.'-'.$year2.'</td>
                      </tr>
                   <tr valign="center"><td height="100px">April '.$year0.'</td><td>March '.$year1.'</td></tr>
                      </table>
     
     </td> 

</tr>
<tr><td>CIT(TDS)<br/>Income Tax office
CR Building,<br/> 
IS Press Road<br/>
City   Kochi Pin 680 018
</td></tr>
</table>';
$head=($balance-$proftax);$agg=0;
$agg=$med+$tr+$inc+$lon+$dle+$totalsection1;
$html.=
'<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr><td align="center"><b>DETAILS OF SALARY PAID AND ANY OTHER INCOME AND TAX DEDUCTED</b></td></tr> 
  <tr><td></td></tr>
</table>
<table width="100%" align="center" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="265" valign="bottom" align="left">1.&nbsp;&nbsp;Gross Salary<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) Salary as per provisions contained in section 17(!)<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b)Value of prequisities under section 17(2)(as per <br/> 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form No,12BA,wherever applicable)<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c)Profits in lieu of salary under section 17(3)<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(as per Form No.12BA,wherever applicable)<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(d)Total<br/>
    2.&nbsp;&nbsp;Less :Allowance to the extent exempt under section 10 <br/>
    3.&nbsp;&nbsp;Balance(1-2)<br/>
    4.&nbsp;&nbsp;Deductions:<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a)Entertainment allowance&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs.<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b)Tax on employment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs'.$proftax.'<br/>   
    5.&nbsp;&nbsp;Aggregate of 4(a) and (b)<br/>
    6.&nbsp;&nbsp;Income chargeable under the head salaries(3-5)<br/>
    7.&nbsp;&nbsp;Add:Any other income reported by the employee<br/>
    8.&nbsp;&nbsp;Gross total income(6+7)<br/>
    9.&nbsp;&nbsp;Deductions under Chapter VIA<br/>
      &nbsp;&nbsp;(A)Sections 80C,80CCC and 80CCD<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gross Amount<br/>
    
   <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr><td>(a)Section 80C</td><td></td></tr><ol>';
    if($licdeduct){ $html.='<tr><td>LIC</td><td>Rs'.$licdeduct.'</td></tr>';}    
    if($nsc1){
    $html.='<tr><td>NSC</td><td>Rs.'.$nsc1.'</td></tr>';}
     if($gpfdeduct){$html.='<tr><td>GPF</td><td>Rs.'.$gpfdeduct.'</td></tr>';}
    if($slitotal){$html.='<tr><td>SLI</td><td>Rs.'.$slitotal.'</td></tr>';}
    if($gistotal){$html.='<tr><td>GIS</td><td>Rs.'.$gistotal.'</td></tr>';}
    if($fbstotal){$html.='<tr><td>FBS</td><td>Rs.'.$fbstotal.'</td></tr>';}
    if($depo){$html.='<tr><td>DEPOSIT</td><td>Rs.'.$depo.'</td></tr>';}
    if($unit){$html.='<tr><td>Contribution to unit linked</td><td>Rs.'.$unit.'</td></tr>';}
    if($plan){$html.='<tr><td>Payment under a contract</td><td>Rs.'.$plan.'</td></tr>';}
    if($mutual){$html.='<tr><td>Purchase of tax saving units</td><td>Rs.'.$mutual.'</td></tr>';}
    if($scheme){$html.='<tr><td>Contribution to deposit scheme</td><td>Rs.'.$scheme.'</td></tr>';}
    if($fee){$html.='<tr><td>Tution Fee</td><td>Rs.'.$fee.'</td></tr>';}
    if($house){$html.='<tr><td>Housing Loan</td><td>Rs'.$house.'</td></tr>';}
    if($eligible){$html.='<tr><td>Subscription to equity shares</td><td>Rs'.$eligible.'</td></tr>';}
    if($fund){$html.='<tr><td>Subscription to mutual fund</td><td>Rs'.$fund.'</td></tr>';}
    if($gpaistotal){$html.='<tr><td>Contribution to GPAIS</td><td>Rs'.$gpaistotal.'</td></tr>';}
    $html.='</table></ol></td>
     <td width="90"><br/>
                    <br/>
                    Rs'.$t_income.'<br/>
                    <br/>
                   <br/><br/><br/><br/>
                    
                    Rs.'.$less.'<br/>
                    <br/><br/><br/><br/>
                    Rs.'.$proftax.'<br/>
                    <br/>
                    <br/>
                    Rs.'.$oth.'<br/>
                    <br/>
                    <br/>
   
                    
                    Qualifying<br/>
                    Amount<br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    
                    </td>
                    
                    
     <td width="90">
     <br/><br/><br/><br/><br/><br/><br/>
     Rs'.$t_income.'<br/>
     <br/>
   
     
     Rs.'.$balance.'<br/><br/>  <br/>
     <br/>
     <br/><br/><br/><br/><br/><br/>Deductible<br/>Amount<br/>
     <br/>
     <br/>';
    $html.='<table width="100%"  border="0" cellspacing="0" cellpadding="0">';
     if($licdeduct){$html.='<tr><td>Rs'.$licdeduct.'</td></tr>';}    
     if($nsc1){$html.='<tr><td>Rs.'.$nsc1.'</td></tr>';}
     if($gpfdeduct){$html.='<tr><td>Rs.'.$gpfdeduct.'</td></tr>';}
    if($slitotal){$html.='<tr><td>Rs.'.$slitotal.'</td></tr>';}
    if($gistotal){$html.='<tr><td>Rs.'.$gistotal.'</td></tr>';}
    if($fbstotal){$html.='<tr><td>Rs.'.$fbstotal.'</td></tr>';}
    if($depo){$html.='<tr><td>Rs.'.$depo.'</td></tr>';}
    if($unit){$html.='<tr><td>Rs.'.$unit.'</td></tr>';}
    if($plan){$html.='<tr><td>Rs.'.$plan.'</td></tr>';}
    if($mutual){$html.='<tr><td>Rs.'.$mutual.'</td></tr>';}
    if($scheme){$html.='<tr><td>Rs.'.$scheme.'</td></tr>';}
    if($fee){$html.='<tr><td>Rs.'.$fee.'</td></tr>';}
    if($house){$html.='<tr><td>Rs'.$house.'</td></tr>';}
    if($eligible){$html.='<tr><td>Rs'.$eligible.'</td></tr>';}
    if($fund){$html.='<tr><td>Rs'.$fund.'</td></tr>';}
    if($gpaistotal){$html.='<tr><td>Rs'.$gpaistotal.'</td></tr>';}
    $html.='</table></td>
     <td width="90">
     <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    
     Rs.'.$head.'<br/>
     <br/>
     
     Rs.'.$head.'<br/>
     </td>
 </tr>
 </table>
 <br/> 
 <br/>
  <br/>
  <br/> 
 <br/>
  <br/>
  <br/>
  <br/> 
 <br/>
<br/>';

   // output the HTML content
$obj_pdf->writeHTML($html, true, false, true, false, '');

// create some HTML content
$html = '
 <table width="100%" align="center" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="265" valign="bottom" align="left">(b)Section 80CCC<br/>
    (c)Section 80CCD<br/>
    (B)Other Sections(for e,g,, 80E,80G,etc.) under Chapter VIA<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Gross Amount<br/><br/>
    <table width="100%"  border="0" cellspacing="0" cellpadding="0">';
    if($total_hba){ $html.='<tr><td>Section 18EE</td><td>Rs'.$total_hba.'</td></tr>';} 
    if($med){ $html.='<tr><td>Section 80D</td><td>Rs'.$med.'</td></tr>';}  
    if($tr){$html.='<tr><td>Section 80DD</td><td>Rs'.$tr.'</td></tr>';}  
    if($inc){ $html.='<tr><td>Section 80DDB</td><td>Rs'.$inc.'</td></tr>';}  
    if($lon){ $html.='<tr><td>Section 80E</td><td>Rs'.$lon.'</td></tr>';} 
    if($dle){ $html.='<tr><td>Section 80U</td><td>Rs'.$dle.'</td></tr>';}
    $html.='</table>10. Aggregate of deductible amount under Chapter VIA<br/>
    11. Total Income(8-10)<br/>
    12. Tax on total Income<br/>
    13.Surcharge (on tax computed at S.No.12)<br/>
    14.Education Cess<br/>(on tax at S.No.12 and surcharge at S.No.13)<br/>
    15.Tax payable(12+13+14)<br/>
    16.Relief under section 89 (attach details)<br/>
    17.Tax payable(15-16)<br/>
    18.Less: (a) Tax deducted at source u/s 192(1)<br/>(b)Tax paid by the employer on behalf of the employee u/s 192(1A) on prequisities u/s 17(2)<br/>
    19.Tax payable /refundable(17-18)<br/>
    </td>
     <td width="90"><br/>
                    <br/>
                    <br/><br/>Qualifying Amount<br/><br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    
     </td>
     <td width="90"><br/>
                    <br/>
                    <br/><br/>Deductible Amount<br/><br/>';
    $html.='<table width="100%"  border="0" cellspacing="0" cellpadding="0">';
                    if($total_hba)
                    {$html.='<tr><td>Rs.'.$total_hba.'</td></tr>';}
                    if($med)  
                      {$html.='<tr><td>Rs.'.$med.'</td></tr>';}
                    if($tr)  
                     {$html.='<tr><td>Rs.'.$tr.'</td></tr>';}
                    if($inc)  
                     {$html.='<tr><td>Rs.'.$inc.'</td></tr>';}
                    if($lon)  
                     {$html.='<tr><td>Rs.'.$lon.'</td></tr>';}
                    if($dle)  
                     {$html.='<tr><td>Rs.'.$dle.'</td></tr>';}
                   $html.='</table><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                    <br/>
                    <br/>
                    Rs.'.$taxtotal.'<br/>
     </td>
     <td width="90">
     <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
     Rs.'.$agg.'<br/>
     Rs.'.$totincome.'<br/>
     Rs.'.$exce.'<br/>
     Rs.'.$surcharge.'<br/>
     <br/>
     Rs.'.$roundcess.'<br/>
     Rs.'.$totaltax.'<br/>
     Rs.'.$rel.'<br/>
     Rs.'.$bal.'<br/>
     <br/>
     <br/>
     Rs.'.$taxtotal.'<br/>
     Rs.'.$tobepaid.'<br/>
     </td>
 </tr>
 </table>
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr><td></td></tr>
  <tr><td align="left"><b>DETAILS OF TAX DEDUCTED AND DEPOSITED INTO CENTRAL GOVERNMENT ACCOUNT</b></td></tr>
</table>
<table width="99%" align="center" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td>SI.No.</td>
    <td>TDS Rs.</td>
    <td>Sur-charge Rs</td>
    <td><p>Education Cess </p>
    <p>Rs.</p></td>
    <td>Total tax depositd Rs.</td>
    <td>Cheque/DD No.(if any)</td>
    <td>BSR Code of Bank branch</td>
    <td>Date on which tax deposited</td>
    <td>Transfer voucher Challan Identification No.</td>
  </tr>
  <tr>
    <td height="90"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br/>
<br/>

<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I ..................................................................son of...........................................................................................</td></tr>
<tr><td>working in the capacity of...................................................................................................................................</td></tr>
<tr><td>(designation)do hereby certify that a sum of Rs ...............................[Rupees..................................................</td></tr>
<tr><td>..................................................................................................in words)] has been deducted at source and</td></tr>
<tr><td> paid to the credit of the Central Government.I further certify that the information given above is true and</td></tr>
<tr><td> correct based on the book of accounts,documents and other available records.</td></tr>
<tr><td></td></tr>
<tr><td align="left">Place.......................................
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.......................................................................................</td>
     </tr>
     <tr>
     <td align="left">Date.......................................
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature of the person responsible for deduction of tax</td>
     </tr>
     <tr>
     <td>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fullname..................................................</td>
     </tr>
     <tr>
     <td>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Designation................................................</td>
     </tr>
</table>';
ob_end_clean();
$obj_pdf->writeHTML($html, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');

?>
