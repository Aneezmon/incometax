<?php

 Class user_model extends CI_Model
 {
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    } 
    function login_Entry($data)//Login Check
    {
		$childuser = NULL;
		$password  = NULL;
		$uq_id = 'not_an_user';
		$child_username = $data[0];
		$child_password = $data[1];
	
		$query = $this->db->get_where('tax_login', array('username' => $child_username,'password'=>$child_password));
		foreach ($query->result() as $row)
					{		
						$childuser = $row->username;
						$password = $row->password ;
						$uq_id=$row->id;
						
					}
		if($child_username === $childuser && $child_password === $password)
			return $query->row_array();
		else
			return false;
		
		

    }
    public function insert($data)//insert details
    {
        $data1=array(
        'pan'=>$data[0],
        'year'=>$data[1],
        'month'=>$data[2],
        'bp'=>$data[3],
        'class'=>$data[4],
        'da'=>$data[5],
        'hra'=>$data[6],
        'cca'=>$data[7],
        'spa'=>$data[8],
        'cona'=>$data[9],
        'hta'=>$data[10],    
        'gross'=>$data[11],    
        'gpf'=>$data[12],
        'sli'=>$data[13],
        'lic'=>$data[14],
        'hba'=>$data[15],
        'fbs'=>$data[16],
        'gis'=>$data[17],
        'interest'=>$data[18],
        'tax'=>$data[19],
        'pli'=>$data[20],
        'gpais'=>$data[21],   
        'deduct'=>$data[22],
         'net'=>$data[23],
         'user'=>$data[24]);
        return $this->db->insert('tax_calc',$data1);
    }
    
    public function check_pan($data)
    {
        $pan=$data[1];
        $this->db->select('*');
	$this->db->where('pan',$pan);
        $query=$this->db->get('tax_calc');
        return $query->result();
    }
    public function match_pan($data)
    {
       $pan=$data[1];
        $this->db->select('*');
	$this->db->where('pan',$pan);
        $query=$this->db->get('tax_calc');
        return $query->result(); 
    }
    public function get_details($data)
    {
        $this->db->select('*');
	$this->db->where('pan',$data[0]);
	$this->db->where('year',$data[1]);
        $this->db->order_by('month', 'asc');
	$query=$this->db->get('tax_calc');
        return $query->result();
    }
     public function insert1($data)//insert details
     {
        
        $data1=array(
         'pan'=>$data['pan'],
        'leave'=>$data[0],
        'fest_allo'=>$data[1],
        'paygpf'=>$data[2],
        'paycash'=>$data[3],
        'daarrier'=>$data[4],
         'daarrier1'=>$data[5],
        'medical'=>$data[6],
       'profe1'=>$data[7],
        'profe2'=>$data[8],    
        'other'=>$data[9],
        'interhba'=>$data[10],
        'licbook'=>$data[11],
        'rent'=>$data[12]);
        return $this->db->insert('tax_input',$data1);
        }
        public function insert2($_POST)
        {
//            print_r($_POST);
            extract($_POST);
            $insert_array=array('mediclaim'=>$mediclaim,
                 'pan'=>$pan,
            'treat'=>$treat,
            'incur'=>$incur,
            'loan'=>$loan,
            'disable'=>$disable,
            'deduct'=>$deduct,
            'deposit'=>$deposit,
             'unit'=>$unit,
             'plan'=>$plan,
             'mutual'=>$mutual,
             'scheme'=>$scheme,
             'fee'=>$fee,
             'house'=>$house,
             'eligible'=>$eligible,
             'fund'=>$fund,
             'nsc'=>$nsc, 
             'releif'=>$releif       
              );
        return $this->db->insert('tax_deduct',$insert_array);
       }
       public function insertemployee($_POST)
       {
           extract($_POST);
            $ins_arr=array('pan'=>$pan,
            'employer_name'=>$name,'address'=>$address,
            'employee_name'=>$emp_name,
            'designation'=>$emp_desig,
            'tan'=>$tan,
            'type'=>$gender);
            
          return $this->db->insert('tax_employee',$ins_arr);   
        }
       function getData($pan)
       {
           $this->db->select('*');
	$this->db->where('pan',$pan);
	
	$query=$this->db->get('tax_calc');
        return $query->result();
       }
        function taxData($pan)
       {
           $this->db->select('*');
	$this->db->where('pan',$pan);
	
	$query=$this->db->get('tax_input');
        return $query->result();
       }
       function hrayear($pan)
       {
           $this->db->select('*');
	$this->db->where('pan',$pan);
	
	$query=$this->db->get('tax_calc');
        return $query->result();
       }
       function taxDeduct($pan)
       {
           $this->db->select('*');
           $this->db->where('pan',$pan);
           
	$query=$this->db->get('tax_deduct');
        return $query->result();
       }
       function taxEmployee($pan)
       {
          $this->db->select('*');
           $this->db->where('pan',$pan);
           
	$query=$this->db->get('tax_employee');
        return $query->result();  
       }

         function get_pan()
       {
         
         $query=$this->db->query("select DISTINCT t.pan from tax_employee t,tax_calc tc where t.pan=tc.pan and tc.user='Ministerial'");
        return $query->result();  
       }
       public function change_psd2($data)
         {
	$new_psd=$data[1];
	$old_psd=$data[0];
	$new_psd=($new_psd);
	$old_psd=($old_psd);
	$datas=array('password'=>$new_psd);
	$child_username_unique_id= $this->session->userdata('child_username_unique_id');
	$res=$this->db->update('tax_login',$datas,array('password'=>$old_psd,'id'=>$child_username_unique_id));
	return mysql_affected_rows();
       }
       public function password_change($data)
	{
		$childuser = NULL;
		$password  = NULL;
		$uq_id = 'not_an_user';
		$child_username = $this->session->userdata('child_username');
		$child_password = $data[0];
		$child_newpsd=$data[1];
//		$email=$data[3]; 
//		$phone=$data[4];
		$datas=array(
		'password'=>$child_newpsd);
//		'status'=>1);
//		'email'=>$email,
//		'phone'=>$phone);
		$query = $this->db->get_where('tax_login', array('username' => $child_username,'password'=>$child_password));
		if($query)
		{
			//echo $query;
			$res1=$this->db->update('tax_login',$datas,array('password'=>$child_password,'username'=>$child_username));
			return mysql_affected_rows();
		}
		else 
		{
				return false;
		}
       
 }
 }
?>
