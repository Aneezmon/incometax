<?php
class ctrl_menu extends CI_Controller
{

  public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('user_model');
        $this->load->helper('form');
        $this->load->library('Session');
    }
    public function add_tax()
    {
        $data1['main'] = 'front/Employee';
	$this->load->vars($data1);
	$this->load->view('front/main'); 
    }
    public function menu()
    {
        $data1['main'] ='front/second';
	$this->load->vars($data1);
	$this->load->view('front/main',$data1); 
    }
    
    public function second()
    {
        $data1['main'] ='front/print_report';
	$this->load->vars($data1);
	$this->load->view('front/main'); 
      }  
      public function edit()
      {
         
        $data1['main'] ='front/edit_report';
	$this->load->vars($data1);
	$this->load->view('front/main');  
      }
    public function excel()
     {
         $data1['main'] ='front/excel_report';
	$this->load->vars($data1);
	$this->load->view('front/main');  
     }   
     public function main()
    {
    $this->load->view('front/main');
    }
    public function report()
    {
      
        $data1['main'] ='front/report';
	$this->load->vars($data1);
	$this->load->view('front/main');   
    }
    public function change_password2() //Change password
	{
		$data1['main'] = 'front/change_password2';
		$this->load->vars($data1);
		$this->load->view('front/main'); 
	}
        public function logout()
	 {
		 $new_child_sess_data = array(
			   'child_username_unique_id'  => '',	//Session variables are userid,username in 	child_username_unique_id and child_username		  
			   'child_username'  => '',					  
			   'logged_in' => FALSE);
				$this->session->set_userdata($new_child_sess_data);
	 	$this->session->all_userdata();
	 	$this->session->sess_destroy();
		$this->load->view('front/login');
	 }
         public function change_psd2()
 {
                $data[0]=$this->input->post('old_psd');
                $data[1]=$this->input->post('new_psd');
                $confirm_psd=$this->input->post('confirm_psd');
                if($data[1]==$confirm_psd)
                {
			$result=$this->user_model->change_psd2($data);
			if($result>0)
		 	{
		 		$data['error']="Password Changed Successfully!";
				$data1['main'] = 'front/change_password2';
				$this->load->vars($data1);
				$this->load->view('front/main',$data); 
		 	}
			else
			{
				$data['error']="Error-Password Doesnot Exist !";
				$data1['main'] = 'front/change_password2';
				$this->load->vars($data1);
				$this->load->view('front/main',$data); 
			}
                }
                else 
                {
			$data['error']="Error-Password Mismatch !";
			$data1['main'] = 'front/change_password2';
			$this->load->vars($data1);
			$this->load->view('front/main',$data); 
                }
	
 }
}
?>
