<?php
class control extends CI_Controller{

    public function __construct() {
        parent::__construct();
       $this->load->helper('url');
       $this->load->model('login_Model');
       $this->load->database();
       $this->load->helper('form');
       $this->load->model('user_model');
       $this->load->library('Session');
       $this->load->library('form_validation');
       $this->load->library('table');
       $this->load->model('model_report');
//                    	$this->load->library('Session');
//				$this->load->library('form_validation');
    }

    public function index()
    {
        $data['error']='';
        $this->load->view('front/login');
    }
    public function second()
    { 
       
        $data['pan']=$this->uri->segment(3);
        $data1['main'] = 'front/second';
	$this->load->vars($data1);
	$this->load->view('front/main',$data); 
    }
    public function third()
    {
       
        $data1['main'] = 'front/third';
	$this->load->vars($data1);
	$this->load->view('front/main'); 
    }
     public function fourth()
    {
        $this->load->view('front/fourth');
        $data1['main'] = 'front/fifth';
	$this->load->vars($data1);
	$this->load->view('front/main',$data); 
    }
    public function fifth()
    {
        $this->load->view('front/fifth');
        $data1['main'] = 'front/fifth';
	$this->load->vars($data1);
	$this->load->view('front/main',$data); 
    }
  public function login()
	{
		
		$this->form_validation->set_rules('password','','trim|required');//Check if password field is empty
		if($this->form_validation->run()==FALSE)//if empty then set a error message
		{
			$data['error']	=	"Sorry, we can't identify your login details !";
			$this->load->view('front/login',$data);
		}
		else
		{
		 $data[0]=$this->input->post('username');
		 $pass=$this->input->post('password');
		 $data[1]=($pass);
		 $this->load->model('user_model');
		 $result = $this->user_model->login_entry($data);
        
		 if($result)
		 {
		 if($data[0] == $result['username'] && $data[1] == $result['password'])
		 	{
		 		$new_child_sess_data = array(
			   'child_username_unique_id'  => $result['id'],					  
			   'child_username'  => $result['username'],					  
			   'logged_in' => TRUE);
				$this->session->set_userdata($new_child_sess_data);
				
		 	//$this->load->view('front/Employee');
                        $data1['main'] ='front/Employee';
                        $this->load->vars($data1);
                        $this->load->view('front/main'); 
                        
		 	}		
			
		 }
		 else
		 {				
				$this->load->view('front/login');
		 }
		 
	}
}	
	public function psdchange() //First Password Change
	{
		$oldpass=$this->input->post('password');
		$data[0]=($oldpass);
		
		$new=$this->input->post('newpassword');
		$data[1]=($new);
		
		$confirm=$this->input->post('confirmpassword');
		$data[2]=($confirm);
//		$data[3]=$this->input->post('email');
//		$data[4]=$this->input->post('phone');
		$this->load->model('user_model');
		if($new==$confirm)
		{
			// $this->session->userdata('child_username'); Retrieve session data
			$result = $this->user_model->password_change($data);
			if($result>0)
			{
				
				$this->load->view('front/main');
			}
			else
			{
				$data['error']	=	"Password does not Exist !";
				$this->load->view('front/changepassword',$data);	
			}
		}
		else
		{
		//PASSWORD MISMATCH	
		$this->load->view('front/changepassword');
		}
		
	}
        
   public function insert()
   {
     
       $data[0]=$this->input->post('pan');
       $data[1]=$this->input->post('year');
       $data[2]=$this->input->post('month');
       $data[3]=$this->input->post('bp');
       $data[4]=$this->input->post('city');
       $data[5]=$this->input->post('da');
       $data[6]=$this->input->post('hra');
       $data[7]=$this->input->post('cca');
       $data[8]=$this->input->post('ca');
       $data[9]=$this->input->post('cona');
       $data[10]=$this->input->post('hta');
       $data[11]=$this->input->post('gross');
       $data[12]=$this->input->post('gp');
       $data[13]=$this->input->post('sli');
       $data[14]=$this->input->post('lic');
       $data[15]=$this->input->post('hba');
       $data[16]=$this->input->post('fbs');
       $data[17]=$this->input->post('gis');
       $data[18]=$this->input->post('inter');
       $data[19]=$this->input->post('tax');
       $data[20]=$this->input->post('pli');
       $data[21]=$this->input->post('gpais');
       $data[22]=$this->input->post('deduct');
       $data[23]=$this->input->post('net');
       $data[24]=$this->input->post('user');
       $res=$this->user_model->insert($data);
       if($res)
       {    
           echo "sucess";
           
       }
       else
           echo "Something went wrong,try again";
       }
public function insert1()
{
       $data[0]=$this->input->post('leave');
       $data[1]=$this->input->post('fest');
       $data[2]=$this->input->post('paygpf');
       $data[3]=$this->input->post('paycash');
       $data[4]=$this->input->post('arrear');
       $data[5]=$this->input->post('arrear1');
       $data[6]=$this->input->post('medical');
//       $data[6]=$this->input->post('cona');
//       $data[7]=$this->input->post('gpais');
       $data[7]=$this->input->post('prof1');
       $data[8]=$this->input->post('prof2');
       $data[9]=$this->input->post('other');
       $data[10]=$this->input->post('interhba');
       $data[11]=$this->input->post('licb');
       $data[12]=$this->input->post('actual');
       $data['pan']=$this->input->post('pan');
      $res=$this->user_model->insert1($data);
       if($res)
       {   
        $data1['main'] = 'front/third';
	$this->load->vars($data1);
	$this->load->view('front/main'); 
       }
       else
       {
        $data1['main'] = 'front/second';
	$this->load->vars($data1);
	$this->load->view('front/main');
        //alert("Please fill all the fields");
       }
}
public function insert2()
{
 if(isset($_POST['submit']))
        {
            $res=$this->user_model->insert2($_POST);
              
            if($res>0)
            {
           // $data['error']="Inserted Successfully";
//            $data1['main'] = 'front/main';
//            $this->load->vars($data1);
//            $this->load->view('front/main'); 
//     $data['pan']=$this->uri->segment(4);
        $this->load->helper('pdf_helper');  
        $data[1]=$this->input->post('pan');
//        $data['deatil']=$this->model_report->tax_calc($data);
//        $data['deatil1']=$this->model_report->tax_input($data);
//        $data['deatil2']=$this->model_report->tax_deduct($data);
//        $data['deatil3']=$this->model_report->tax_employee($data);
      //$this->load->view('pdfreport',$data);
     $pan=$this->session->userdata('panNumber');
         $data['deatils']=$this->user_model->getData($pan);
       $data['deatils1']=$this->user_model->taxData($pan);
       $data['deatils2']=$this->user_model->taxDeduct($pan); 
       $data['deatils3']=$this->user_model->taxEmployee($pan); 
$this->session->unset_userdata('panNumber');
        $data1['main'] = 'front/designform1';
            $this->load->vars($data1);
            $this->load->view('front/main',$data);  
            }
       
          else
           {
            $data['error']="Not inserted";
            $data1['main'] = 'front/third';
            $this->load->vars($data1);
            $this->load->view('front/home',$data); 
            }
       }
 
  else {
          $data1['main'] = 'front/third';
            $this->load->vars($data1);
            $this->load->view('front/main',$data1);    
     
       }       
    
}
public function insertemployee()
{
 if(isset($_POST['submit']))
        {
            $res=$this->user_model->insertemployee($_POST);
            if($res)
            {
            extract($_POST);
	    $this->session->set_userdata(array('panNumber'=>$pan));
            $this->session->set_userdata(array('Name'=>$emp_name));
            $this->session->set_userdata(array('Desig'=>$emp_desig));
           $data1['main'] = 'front/first';
           $this->load->vars($data1);
	   $this->load->view('front/main'); 
        
            }
//            else
//            {
//             $data['error']="Not inserted";
//             $this->load->view('front/Employee');
//            }       
      }
}

public function update()
{
   
    if(isset($_POST['submit']))
    {
        
     $res=$this->model_report->update($_POST); 
//    if($res)
//    {
////       $data['result']="Updated successfully";
//         
////        $data1['main'] ='front/edit_report';
////       $this->load->vars($data1);
////       $this->load->view('front/main');
////        $data[1]=$this->input->post('seritem');
        $data[1]=$this->input->post('pan');
//      $res1=$this->user_model->check_pan($data);
//       
           
          $data['id']=$data[1];
          $data['deatil3']=$this->model_report->tax_deduct($data);  
          $data1['main'] ='front/edit_report2';
          $this->load->vars($data1);
          $this->load->view('front/main',$data);
       
//    }
  }
}
public function update1()
{
    if(isset($_POST['submit']))
    {
        
     $res=$this->model_report->update1($_POST); 
     if($res)
     {
       $data['error']="Updated successfully";
       $data1['main'] ='front/edit_report';
       $this->load->vars($data1);
       $this->load->view('front/main',$data);
     }
    }
}
// public function second_page()
//        {
//     $data1['main'] = 'front/first_old';
//	$this->load->vars($data1);
//	$this->load->view('front/main'); 
//         }
//public function  third_page()
//{
// $data1['main'] = 'front/second';
//	$this->load->vars($data1);
//	$this->load->view('front/main');    
//}
public function get_details()
{
    $data[0]=$_POST['panval'];//panvalue
    $data[1]=$_POST['yearval'];//yearil.com
    $res=$this->user_model->get_details($data);
    $i=0;
   $f="";
    foreach($res as $row)
    {
        $j=$i+1;
     $mon='';
        switch($row->month)
        {
            case 1:
                $mon='March';
                break;
            case 2:
                $mon='April';
                break;
            case 3:
                $mon='May';
                break;
             case 4:
                $mon='June';
                break;
            case 5:
                $mon='July';
                break;
            case 6:
                $mon='August';
                break;
             case 7:
                $mon='September';
                break;
            case 8:
                $mon='October';
                break;
            case 9:
                $mon='November';
                break;
             case 10:
                $mon='December';
                break;
            case 11:
                $mon='January';
                break;
            case 12:
                $mon='February';
                break;    
            
        }
    $f.= "<tr id='append".$j."'><td><input type='hidden' name='month".$i."' value='".$row->month."'>".$mon."</td><td><input type='hidden' name='bp".$i."' value='".$row->bp."'>".$row->bp."</td><td><input type='hidden' name='city".$i."' value='".$row->class."'>".$row->class."</td><td><input type='hidden' name='da".$i."' value='".$row->da."'>".$row->da."</td><td><input type='hidden' name='hra".$i."' value='".$row->hra."'>".$row->hra."</td><td><input type='hidden' name='cca".$i."' value='".$row->cca."'>".$row->cca."</td><td><input type='hidden' name='ca".$i."' value='".$row->spa."'>".$row->spa."</td><td><input type='hidden' name='cona".$i."' value='".$row->cona."'>".$row->cona."</td><td><td><input type='hidden' name='hta".$i."' value='".$row->hta."'>".$row->hta."</td><td><input type='hidden' name='gross".$i."' value='".$row->gross."'>".$row->gross."</td><td><input type='hidden' name='gp".$i."' value='".$row->gpf."'>".$row->gpf."</td><td><input type='hidden' name='sli".$i."' value='".$row->sli."'>".$row->sli."</td><td><input type='hidden' name='lic".$i."' value='".$row->lic."'>".$row->lic."</td><td><input type='hidden' name='hba".$i."' value='".$row->hba."'>".$row->hba."</td><td><input type='hidden' name='fbs".$i."' value='".$row->fbs."'>".$row->fbs."</td><td><input type='hidden' name='gis".$i."' value='".$row->gis."'>".$row->gis."</td><td><input type='hidden' name='inter".$i."' value='".$row->Interest."'>".$row->Interest."</td><td><input type='hidden' name='tax".$i."' value='".$row->tax."'>".$row->tax."</td><td><input type='hidden' name='pli".$i."' value='".$row->pli."'>".$row->pli."</td><td><input type='hidden' name='gpais".$i."' value='".$row->gpais."'>".$row->gpais."</td><td><input type='hidden' name='deduct".$i."' value='".$row->deduct."'>".$row->deduct."</td><td><input type='hidden' name='net".$i."' value='".$row->net."'>".$row->net."</td></tr>";
    $i++;  
    } 
$arr=array('row'=>$f,'i'=>$i);
//print_r($arr);
echo json_encode($arr);
 }
    

}
    